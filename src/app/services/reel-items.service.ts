import { Injectable } from '@angular/core';
import { Fruit } from '../enums';
import { ReelItem } from '../models';

@Injectable()
export class ReelItemsService {
    public getReel1Items(): ReelItem[] {
        return [
            {
                id: 0,
                name: 'CHERRY',
                fruitType: Fruit.CHERRY,
                image: 'assets/icons/cherries.png'
            },
            {
                id: 1,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 2,
                name: 'APPLE',
                fruitType: Fruit.APPLE,
                image: 'assets/icons/apple.png'
            },
            {
                id: 3,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 4,
                name: 'BANANA',
                fruitType: Fruit.BANANA,
                image: 'assets/icons/banana.png'
            },
            {
                id: 5,
                name: 'BANANA',
                fruitType: Fruit.BANANA,
                image: 'assets/icons/banana.png'
            },
            {
                id: 6,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 7,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            }
        ];
    }

    public getReel2Items(): ReelItem[] {
        return [
            {
                id: 0,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 1,
                name: 'APPLE',
                fruitType: Fruit.APPLE,
                image: 'assets/icons/apple.png'
            },
            {
                id: 2,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 3,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 4,
                name: 'CHERRY',
                fruitType: Fruit.CHERRY,
                image: 'assets/icons/cherries.png'
            },
            {
                id: 5,
                name: 'APPLE',
                fruitType: Fruit.APPLE,
                image: 'assets/icons/apple.png'
            },
            {
                id: 6,
                name: 'BANANA',
                fruitType: Fruit.BANANA,
                image: 'assets/icons/banana.png'
            },
            {
                id: 7,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            }
        ];
    }

    public getReel3Items(): ReelItem[] {
        return [
            {
                id: 0,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 1,
                name: 'APPLE',
                fruitType: Fruit.APPLE,
                image: 'assets/icons/apple.png'
            },
            {
                id: 2,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 3,
                name: 'APPLE',
                fruitType: Fruit.APPLE,
                image: 'assets/icons/apple.png'
            },
            {
                id: 4,
                name: 'CHERRY',
                fruitType: Fruit.CHERRY,
                image: 'assets/icons/cherries.png'
            },
            {
                id: 5,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            },
            {
                id: 6,
                name: 'BANANA',
                fruitType: Fruit.BANANA,
                image: 'assets/icons/banana.png'
            },
            {
                id: 7,
                name: 'LEMON',
                fruitType: Fruit.LEMON,
                image: 'assets/icons/lemon.png'
            }
        ];
    }
}
