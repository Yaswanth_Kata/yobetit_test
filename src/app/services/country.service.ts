import { Injectable } from '@angular/core';
import { HttpProvider } from '../providers/http.provider';
import { Country } from '../models';

@Injectable()
export class CountryService {
  constructor(private httpProvider: HttpProvider) {}

  public getCountries() {
    return this.httpProvider.get<Country[]>('all').toPromise();
  }
}
