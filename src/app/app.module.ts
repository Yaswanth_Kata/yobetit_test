import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NodejsComponent } from './nodejs/nodejs.component';
import { SqlComponent } from './sql/sql.component';
import { HttpProvider } from './providers/http.provider';
import { CountryService } from './services/country.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing.module';
import { MaterialModule } from './material.module';
import { ReelItemsService } from './services';

const providers = [HttpProvider];
const services = [CountryService, ReelItemsService];

@NgModule({
  declarations: [AppComponent, NodejsComponent, SqlComponent],
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [...services, ...providers],
  bootstrap: [AppComponent]
})
export class AppModule { }
