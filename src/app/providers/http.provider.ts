import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class HttpProvider {
  public baseUrl: string | null = 'https://restcountries.eu/rest/v2/';
  public ticketKey: string | null = null;

  constructor(private http: HttpClient) {}

  public formatUrl(relativeUrl: string): string {
    if (!relativeUrl) {
      throw new Error('missing relativeUrl');
    }

    if (this.baseUrl) {
      return this.baseUrl + relativeUrl;
    }

    return relativeUrl;
  }

  public get<T>(relativeUrl: string, data?: any): Observable<T> {
    relativeUrl = this.formatUrl(relativeUrl);
    return this.request<T>(relativeUrl, 'Get', data, false);
  }

  public request<T>(
    relativeUrl: string,
    method: string,
    data?: any,
    formatUrl: boolean = true
  ): Observable<T> {
    if (formatUrl) {
      relativeUrl = this.formatUrl(relativeUrl);
    }

    return this.http
      .request(method, relativeUrl, { body: data, responseType: 'text' })
      .pipe(
        map(res => {
          if (!res) {
            return <any>res;
          }

          return <T>JSON.parse(res);
        })
      );
  }
}
