import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { NodejsComponent } from './nodejs/nodejs.component';
import { SqlComponent } from './sql/sql.component';

const routes: Routes = [
  {
    path: '',
    component: NodejsComponent
  },
  {
    path: 'nodejs',
    component: NodejsComponent
  },
  {
    path: 'sql',
    component: SqlComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
