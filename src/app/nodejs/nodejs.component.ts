import { Component, OnInit } from '@angular/core';
import { CountryService } from '../services/country.service';
import { Country } from '../models';
import { FormControl, Validators } from '@angular/forms';
import { Fruit, SlotType } from '../enums';
import { ReelItem } from '../models/reel-item.model';
import { ReelItemsService } from '../services';

@Component({
  selector: 'app-nodejs',
  templateUrl: './nodejs.component.html',
  styleUrls: ['./nodejs.component.scss']
})
export class NodejsComponent implements OnInit {
  countries: Country[] = null;
  // question1 control
  digitInputControl = new FormControl(null, [Validators.required]);
  // question2 control
  countryNameInputControl = new FormControl('', [Validators.required]);

  // question3 control
  inputStringsArray = new FormControl('', [Validators.required]);

  // dropdown controls for spin
  reel1SelectControl = new FormControl();
  reel2SelectControl = new FormControl();
  reel3SelectControl = new FormControl();

  reel1Selected = 0;
  reel2Selected = 0;
  reel3Selected = 0;

  // variables to hold reel random vlaues
  reel1RandomValue: string = null;
  reel2RandomValue: string = null;
  reel3RandomValue: string = null;

  isErrorEnabled = false;

  selectedSpinStyle = SlotType.MANUAL;

  countryMatches = new Map<string, Country[]>();

  selectedCountry = new Country();
  nth_DigitOfPi = null;
  coinsWon = null;
  fruitCountMap = new Map<Fruit, number>();

  // array to hold reelitems
  reel1Items: ReelItem[] = [];
  reel2Items: ReelItem[] = [];
  reel3Items: ReelItem[] = [];
  constructor(
    private countryService: CountryService,
    private reelItemsService: ReelItemsService
  ) {}

  getErrorMessage() {
    return this.countryNameInputControl.hasError('required')
      ? 'You must enter a value'
      : '';
  }
  async ngOnInit() {
    this.buildReels();
    this.fruitCountMap.clear();
    this.countries = await this.countryService.getCountries();
  }

  buildReels() {
    this.reel1Items = this.reelItemsService.getReel1Items();
    this.reel2Items = this.reelItemsService.getReel2Items();
    this.reel3Items = this.reelItemsService.getReel3Items();
  }

  getUniqueCountry(countryname) {
    if (this.countries.length) {
      const filteredCountry = this.countries.find(
        c => c.name.toLowerCase() === countryname.toLowerCase()
      );
      if (filteredCountry) {
        this.selectedCountry = filteredCountry;
      } else {
        this.selectedCountry = null;
        console.log('No country found');
      }
    }
  }

  getCountriesBasedonArray(countrynames: string) {
    this.countryMatches.clear();
    if (countrynames.length) {
      const namesList = countrynames.split(',').filter(cn => cn !== '');
      for (let i = 0; i <= namesList.length - 1; i++) {
        // filter the countries array based on the string
        const filteredCountries = this.countries.filter(
          c => namesList[i].toLowerCase().indexOf(c.name.toLowerCase()) !== -1
        );
        if (filteredCountries.length) {
          this.isErrorEnabled = false;
          this.countryMatches.set(namesList[i], filteredCountries);
        } else {
          this.isErrorEnabled = true;
        }
      }
    }
  }

  getNthDigit(n) {
    let s = 0;
    s += 4 * this.getPartial(n, 1);
    s += -2 * this.getPartial(n, 4);
    s += -1 * this.getPartial(n, 5);
    s += -1 * this.getPartial(n, 6);

    s = s < 0 ? 1 - (-s % 1) : s % 1;
    this.nth_DigitOfPi = Math.floor(s * 16);
  }

  getPartial(d, c) {
    let sum = 0;

    // Left sum
    for (let k = 0; k <= d - 1; k++) {
      sum += (Math.pow(16, d - 1 - k) % (8 * k + c)) / (8 * k + c);
    }

    // Right sum.
    let prev;
    for (let k = d; sum !== prev; k++) {
      prev = sum;
      sum += Math.pow(16, d - 1 - k) / (8 * k + c);
    }

    return sum;
  }

  spin() {
    // generate random integers between 0-8
    const reel1Random = Math.floor(Math.random() * this.reel1Items.length);
    const reel2Random = Math.floor(Math.random() * this.reel2Items.length);
    const reel3Random = Math.floor(Math.random() * this.reel3Items.length);

    // select the fruit type based on index
    const reel1RandomFruit = this.reel1Items.find(r => r.id === reel1Random)
      .fruitType;
    const reel2RandomFruit = this.reel2Items.find(r => r.id === reel2Random)
      .fruitType;
    const reel3RandomFruit = this.reel3Items.find(r => r.id === reel3Random)
      .fruitType;

    this.reel1RandomValue = Fruit[reel1RandomFruit];
    this.reel2RandomValue = Fruit[reel2RandomFruit];
    this.reel3RandomValue = Fruit[reel3RandomFruit];

    this.getSpinResult(reel1RandomFruit, reel2RandomFruit, reel3RandomFruit);
  }

  getSpinResult(reel1, reel2, reel3) {
    this.fruitCountMap.clear();

    console.log(reel1, reel2, reel3);
    this.fruitCountMap.set(reel1, 0);
    this.fruitCountMap.set(reel2, 0);
    this.fruitCountMap.set(reel3, 0);
    // storing the count of the fruit in hash
    this.fruitCountMap.set(reel1, this.fruitCountMap.get(reel1) + 1);
    this.fruitCountMap.set(reel2, this.fruitCountMap.get(reel2) + 1);
    this.fruitCountMap.set(reel3, this.fruitCountMap.get(reel3) + 1);

    // Output
    if (this.fruitCountMap.get(Fruit.CHERRY) === 3) {
      this.coinsWon = 50;
    } else if (this.fruitCountMap.get(Fruit.CHERRY) === 2) {
      this.coinsWon = 40;
    } else if (this.fruitCountMap.get(Fruit.APPLE) === 3) {
      this.coinsWon = 20;
    } else if (this.fruitCountMap.get(Fruit.APPLE) === 2) {
      this.coinsWon = 10;
    } else if (this.fruitCountMap.get(Fruit.BANANA) === 3) {
      this.coinsWon = 15;
    } else if (this.fruitCountMap.get(Fruit.BANANA) === 2) {
      this.coinsWon = 5;
    } else if (this.fruitCountMap.get(Fruit.LEMON) === 3) {
      this.coinsWon = 3;
    } else {
      this.coinsWon = 0;
    }
  }

  OnReel1SelectionChange(event) {
    this.reel1Selected = event.value;
  }
  OnReel2SelectionChange(event) {
    this.reel2Selected = event.value;
  }
  OnReel3SelectionChange(event) {
    this.reel3Selected = event.value;
  }

  onRadioChange(spin) {
    this.coinsWon = null;
    this.selectedSpinStyle = spin;
  }

  checkResult() {
    this.getSpinResult(
      this.reel1Selected,
      this.reel2Selected,
      this.reel3Selected
    );
  }
}
