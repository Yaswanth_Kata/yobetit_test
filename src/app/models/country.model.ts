export class Country {
  name: string = null;
  capital: string = null;
  region: string = null;
  flag: string = null;
}
