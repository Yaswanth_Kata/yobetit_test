import { Fruit } from '../enums';

export interface ReelItem {
    id: number;
    name: string;
    image: string;
    fruitType: Fruit;
}
